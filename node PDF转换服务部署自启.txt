﻿npm install pm2 -g

npm install pm2-windows-startup -g

pm2-startup install
全局安装以上

pm2 start 路径 --name 名称

例: pm2 start d:/covert-pdf/app.js --name ServiceCovertPdf
保存守护进程
pm2 save

pm2 start app.js  --name ServiceCovertPdf  --watch  

pm2 start       # 启动一个服务，携带 --name 参数添加一个应用名，携带参数 --watch 将观察修改重启服务
pm2 list        # 列出当前的服务
pm2 monit       # 监视每个node进程的CPU和内存的使用情况
pm2 stop 0      # 停止服务(pm2 stop 名称或id)
pm2 stop all    # 停止所有服务进程
pm2 restart 0   # 重启服务(pm2 restart app.js)
pm2 restart all # 重启所有进程
pm2 delete 0    # 删除服务(pm2 delete app_name|app_id)
pm2 delete all  # 删除全部服务
pm2 logs        # 查看所有服务的输出日志
pm2 logs 0      # 查看服务的输出日志

